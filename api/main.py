import os

from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import FileResponse
from submission import ResultsHandler
from pathlib import Path
import requests
import json
import yaml
import copy

from typing import List
from pydantic import BaseModel


# Starts Api Server
app = FastAPI()
#game_results_handler = ResultsHandler()

allowed_origin = os.getenv("GODOT_PYTHON_URI") if os.getenv("GODOT_PYTHON_URI") else 'http://localhost:8000'#"127.0.0.1:8000"
user_code_uri = os.getenv("USER_CODE_URI") if os.getenv("USER_CODE_URI") else "../user-code"
scenario_path = './../../LightingBinding/ScenarioFiles/'
riders_scenario_path = '/workspace/src/testing/scenarios/'#user_code_uri + "scenario_files/" 

scenario_path = riders_scenario_path if os.getenv("GODOT_PYTHON_URI") else scenario_path


cezeri_template = {
    "name": "name",
    "return_to_start": False,
    "battery": 100,
    "max_charge": 10,
    "start": {
        "name": "start_point",
        "helipad_choices": [1]
    },
    "destinations": [],
    "triggers": []

}

destination_template = {
    "name": "dest_b",
    "helipad_choices": [1],
    "task": "visit",
    "order": 0
}

trigger_template = {
    "type": "error",
    "referance_point": "start_point",
    "min_distance": 10,
    "max_distance": 20,
    
    "sensor": "gnss",
    "switch_destination_name": "dest_b",
    "new_destination": 0
}

weather_template = {
    "name": "rain_1",
    "type": "rain",
    "start_point": "start_point",
    "end_point": "dest_b",
    "count": 2
}

traffic_template = {
    "name": "traffic_1",
    "start_point": "start_point",
    "end_point": "dest_b",
}

slow_zone_template = {
    "name": "slow_1",
    "start_point": "start_point",
    "end_point": "dest_b",
}


async def get_target_name_by_helipad(helipad_id, cezeri):
    target = None
    if cezeri["start"]["helipad_choices"][0] == helipad_id:
        target = cezeri["start"]["name"]

    if cezeri["start"]["helipad_choices"][0] == helipad_id:
        target = cezeri["start"]["name"]

    for destination in cezeri["destinations"]:
        if destination["helipad_choices"][0] == helipad_id:
            target = destination["name"]

        if destination["helipad_choices"][0] == helipad_id:
            target = destination["name"]
    
    if target == None:
        print("no target found for helipad: " + str(helipad_id))
    
    return target


async def convert_result(data):
    print(yaml.dump(data))

    scenario = {
        "name": data.get("scenario_name", ""),
        "cezeri_missions": [],
        "scenario_events": {
            "weather": [],
            "traffic": [],
            "slow_zone": []
        },
    }
    
    for cezeri_id in data:
        if cezeri_id == "scenario_name":
            continue
        cezeri_setting = data[cezeri_id]

        cezeri = copy.deepcopy(cezeri_template)
        
        # cezeri settings
        cezeri["name"] = cezeri_id
        cezeri["return_to_start"] = cezeri_setting.get("return_to_start", False)
        cezeri["battery"] = cezeri_setting["settings"].get("battery", 100)
        cezeri["max_charge"] = cezeri_setting["settings"].get("count", 10)
        
        # start point_settings
        cezeri["start"]["name"] = cezeri_id + "_Baslangic"
        cezeri["start"]["helipad_choices"] = [cezeri_setting.get("start_point", 1)]
        
        # destination settings
        for target_id in cezeri_setting["target_points"]:
            target_setting = cezeri_setting["target_points"][target_id]
            destination = copy.deepcopy(destination_template)
            
            destination["name"] = cezeri_id + "_" + target_id
            destination["helipad_choices"] = [target_setting.get("point", 0)]
            destination["score"] = target_setting.get("score", 0)
                        
            order = int(target_setting.get("order", 0))
            if order == -1:
                 destination["order"] = 0
            else:
                 destination["order"] = order

            
            mission = target_setting.get("mission", "land")

            if mission == "Inis":
                destination["task"] = "land"
            else:
                destination["task"] = "visit"

            cezeri["destinations"].append(destination)
            
            
        # trigger settings
        for trigger_id in cezeri_setting["trigger"]:
            trigger_setting = cezeri_setting["trigger"][trigger_id]
            trigger = copy.deepcopy(trigger_template)
            
            trigger_type = trigger_setting.get("type")

            if trigger_type == "Acil Durum":
                trigger["type"] = "emergency"
            elif trigger_type == "Gurultu":
                trigger["type"] = "mix"
                trigger["sensor"] = trigger_setting.get("sensor")
                if trigger["sensor"] == "magnetometre":
                    trigger["sensor"] = "magnetometer"
                elif trigger["sensor"] == "barometre":
                    trigger["sensor"] = "barometer"
                if trigger["sensor"] == " gnss":
                    trigger["sensor"] = "gnss"
                else:
                    trigger["sensor"] = trigger["sensor"].lower()
            elif trigger_type == "Hata":
                trigger["type"] = "error"
                trigger["sensor"] = trigger_setting.get("sensor")
                if trigger["sensor"] == "magnetometre":
                    trigger["sensor"] = "magnetometer"
                elif trigger["sensor"] == "barometre":
                    trigger["sensor"] = "barometer"
                elif trigger["sensor"] == " gnss":
                    trigger["sensor"] = "gnss"
                else:
                    trigger["sensor"] = trigger["sensor"].lower()
            elif trigger_type == "Hedef Degistirme":
                trigger["type"] = "destination_switch"
                trigger["switch_destination_name"] = cezeri_id + "_" + trigger_setting.get("switch_destination_name")
                trigger["new_destination"] = int(trigger_setting.get("new_destination"))
                #trigger["switch_destination_name"] = await get_target_name_by_helipad(int(trigger_setting.get("switch_destination_name")), cezeri)
            
            trigger["referance_point"] = cezeri_id + "_" + trigger_setting.get("point")
            trigger["min_distance"] = trigger_setting.get("max_distance", 10)
            trigger["max_distance"] = trigger_setting.get("min_distance", 20)
            
            cezeri["triggers"].append(trigger)
            
            
        # weather settings
        for weather_id in cezeri_setting["weather"]:
            weather_setting = cezeri_setting["weather"][weather_id]
            weather = copy.deepcopy(weather_template)
            
            
            weather["name"] = cezeri_id + "_" + weather_id
            weather["type"] = weather_setting.get("type", "rain")
            weather["count"] = weather_setting.get("count", 1)
            weather["start_point"] = cezeri_id + "_" + weather_setting.get("start_point")
            weather["end_point"] = cezeri_id + "_" + weather_setting.get("end_point")
            
            scenario["scenario_events"]["weather"].append(weather)
            
        
        # traffic settings
        for traffic_id in cezeri_setting["traffic"]:
            traffic_setting = cezeri_setting["traffic"][traffic_id]
            traffic = copy.deepcopy(traffic_template)
            
            
            traffic["name"] = cezeri_id + "_" + traffic_id
            traffic["start_point"] = cezeri_id + "_" + traffic_setting.get("start_point")
            traffic["end_point"] = cezeri_id + "_" + traffic_setting.get("end_point")
            
            scenario["scenario_events"]["traffic"].append(traffic)
            
        
        # slow_zone settings
        for slow_zone_id in cezeri_setting["slow_area"]:
            slow_zone_setting = cezeri_setting["slow_area"][slow_zone_id]
            slow_zone = copy.deepcopy(slow_zone_template)

            slow_zone["name"] = cezeri_id + "_" + slow_zone_id
            slow_zone["start_point"] = cezeri_id + "_" + slow_zone_setting.get("start_point")
            slow_zone["end_point"] = cezeri_id + "_" + slow_zone_setting.get("end_point")

            scenario["scenario_events"]["slow_zone"].append(slow_zone)
            

        scenario["cezeri_missions"].append(cezeri)
    

    file = open(riders_scenario_path + scenario["name"]+'.yaml','w')
    yaml.dump(scenario, file) #scenario["name"]
    #print("--------------------------------------------------------")
    #print(yaml.dump(scenario))



@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    response = await call_next(request)
    response.headers["Cross-Origin-Opener-Policy"] = "same-origin"
    response.headers["Cross-Origin-Embedder-Policy"] = "require-corp"
    response.headers["Cross-Origin-Resource-Policy"] = "cross-origin"
    return response


@app.get("/get-file/{file_name}")
async def get_map(file_name):
    print("get method worked")
    

@app.post("/send-file/")
async def send_map(req:Request):
    print("send_method worked")
    result = await req.json()
    await convert_result(result)
    return {"status":"SUCCESS"}


# Serves static files contained in export folder to client.

# app.mount("/user-code", StaticFiles(directory= user_code_uri), name="user-code-file-server")
if "RIDERS_HOST" in os.environ:
    app.mount("/project", StaticFiles(directory= "/workspace/src/_lib/"), name="project-materials-server")
app.mount("/", StaticFiles(directory= "../godot-web-export"), name="godot-web-file-server")


