#!/bin/bash

export GODOT_PYTHON_PORT=8000
export GODOT_PYTHON_URI="$(ride_ports_get_address $GODOT_PYTHON_PORT)"

export GODOT_WEB_URI="$GODOT_PYTHON_URI/index.html"
export GODOT_MAP_URI="$GODOT_PYTHON_URI/map.html"

export USER_CODE_URI="${RIDE_SOURCE_PATH}/user_code/"