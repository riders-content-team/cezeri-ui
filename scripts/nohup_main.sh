source "${RIDERS_GLOBAL_PATH}/lib/helpers.sh"

echo -e "$GREEN"
echo " --------------------"
echo "| Proje Hazırlanıyor |"
echo " --------------------"
echo ""

# install requests package
python3 -m pip install --upgrade pip
python3 -m pip install requests
python3 -m pip install "fastapi[all]"

# Setup Godot and FastApi
source "${RIDE_PACKAGES_PATH}/cezeri_sandbox/scripts/godot_setup.sh"
sleep 1
source "${RIDE_PACKAGES_PATH}/cezeri_sandbox/scripts/api_setup.sh"

# Show Simulation and Blockly in Iframe
#source "${RIDE_SOURCE_RIDERS_PATH}/commands/b_open_blockly_code.sh"
#sleep 1
#source "${RIDE_SOURCE_RIDERS_PATH}/commands/1-Editor/01_Start.sh"
